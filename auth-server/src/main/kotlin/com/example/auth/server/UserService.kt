package com.example.auth.server

class UserService {

    private val savedUsers = listOf(
            User("admin", "admin"),
            User("user", "password")
    )

    fun allowedEntryFor(user: User): Boolean = savedUsers.contains(user)
}