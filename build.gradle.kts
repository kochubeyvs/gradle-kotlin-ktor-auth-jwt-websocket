val kotlinVersion: String by project
val ktorVersion: String by project
val coroutinesVersion: String by project
val logbackVersion: String by project

plugins {
    application
    kotlin("jvm")
    id("org.jetbrains.kotlin.plugin.serialization")
}

group = "com.example.auth.jwt"
version = "0.0.1"

subprojects {
    group = rootProject.group
    version = rootProject.version

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = "11"
    }

    repositories {
        mavenCentral()
    }
}
