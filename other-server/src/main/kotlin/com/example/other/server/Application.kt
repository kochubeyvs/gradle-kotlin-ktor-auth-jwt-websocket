package com.example.other.server

import com.auth0.jwt.*
import com.auth0.jwt.algorithms.*
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.websocket.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.http.auth.*
import io.ktor.http.cio.websocket.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.serialization.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.launch

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

fun Application.module() {
    val secret = environment.config.property("jwt.secret").getString()
    val issuer = environment.config.property("jwt.issuer").getString()
    val audience = environment.config.property("jwt.audience").getString()
    val myRealm = environment.config.property("jwt.realm").getString()

    install(ContentNegotiation) { json() }

    install(WebSockets)

    install(Authentication) {
        jwt("auth-jwt-http") {
            realm = myRealm
            verifier(
                JWT.require(Algorithm.HMAC256(secret))
                    .withAudience(audience)
                    .withIssuer(issuer)
                    .build()
            )
            validate { credential ->
                if (credential.payload.getClaim("name").asString() != "") {
                    JWTPrincipal(credential.payload)
                } else {
                    null
                }
            }
        }

        jwt("auth-jwt-ws") {
            realm = myRealm
            verifier(
                JWT.require(Algorithm.HMAC256(secret))
                    .withAudience(audience)
                    .withIssuer(issuer)
                    .build()
            )
            validate { credential ->
                if (credential.payload.getClaim("name").asString() != "") {
                    JWTPrincipal(credential.payload)
                } else {
                    null
                }
            }
            authHeader {
                parseAuthorizationHeader("Bearer ${it.parameters["token"]}")
            }
        }
    }

    routing {

        // no auth zone

        get("/http") {
            call.respond("Got GET request on /http")
        }

        webSocket("/ws") {
            incoming.consumeAsFlow()
                .mapNotNull {
                    (it as? Frame.Text)?.readText()
                }
                .collect {
                    send(Frame.Text("Got websocket message: $it"))
                }
        }

        // auth zone

        authenticate("auth-jwt-http") {
            get("/auth/http") {
                val principal = call.authentication.principal<JWTPrincipal>()
                val name = principal?.payload?.getClaim("name")?.asString()
                val expiresAt = principal?.expiresAt?.time?.minus(System.currentTimeMillis())
                call.respondText("Hello, $name! Token is expired at $expiresAt ms")
            }
        }

        authenticate("auth-jwt-ws") {
            webSocket("/auth/ws") {
                val principal = call.principal<JWTPrincipal>()
                val name = principal?.payload?.getClaim("name")?.asString()

                launch {
                    val expiresAt = principal?.expiresAt?.time?.minus(System.currentTimeMillis()) ?: 0
                    delay(expiresAt)
                    close(CloseReason(1, "Hello, $name! Sorry, but token is expired, try to get new one"))
                }

                incoming.consumeAsFlow()
                    .mapNotNull {
                        (it as? Frame.Text)?.readText()
                    }
                    .collect {
                        val expiresAt = principal?.expiresAt?.time?.minus(System.currentTimeMillis())
                        send(Frame.Text("Got message from $name, token is expired at $expiresAt ms, message text: $it"))
                    }
            }
        }
    }
}
