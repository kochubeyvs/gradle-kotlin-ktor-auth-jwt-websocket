rootProject.name = "auth-jwt"

pluginManagement {
    val kotlinVersion: String by settings

    plugins {
        kotlin("jvm") version kotlinVersion apply false
        id("org.jetbrains.kotlin.plugin.serialization") version kotlinVersion apply false
    }
}

include(":auth-server")
include(":other-server")
include(":http-client")